CREATE DATABASE test;
CREATE USER 'user'@'localhost' identified by 'password';
GRANT ALL PRIVILEGES ON test.* TO 'user'@'localhost';
