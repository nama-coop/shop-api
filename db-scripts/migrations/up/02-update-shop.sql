RENAME TABLE Shops TO shop;
ALTER TABLE shop
CHANGE COLUMN latitute latitude VARCHAR(255),
ADD COLUMN nama_shop_id INT,
ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
