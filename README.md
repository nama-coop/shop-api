# shop-api

REST API that serves shop information.

Hosted at: [http://api.nachbarschaftsmarktplatz.de](http://api.nachbarschaftsmarktplatz.de)

### Description
There are two endpoints:

```text
// return all records
GET /shop

// create a new record
POST /shop
```

### Setup mysql
* Install mySQL: `sudo apt install mysql-server`
* Setup test database: `sudo mysql < db-scripts/setup.sql`
* Login as test user: `mysql test -uuser -ppassword`
* Run migrations: `SOURCE db-scripts/migrations/up/01_create_shop.sql;`, etc...
* Seed database: `SOURCE db-scripts/seed.sql;`

### Setup php and composer (package manager)
* [Follow this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04)
* Run: `composer install`
* Fill `.env` with your credentials or use default: `cp .env.dev .env`
* Start dev server: `php -S localhost:8001 -t public`

### Misc
* [Create you own Bearer token](https://codepen.io/corenominal/pen/rxOmMJ) for authentication.
* Check out the [postman](https://www.postman.com/) collection in test folder.
* Skeleton taken from [this blog post](https://developer.okta.com/blog/2019/03/08/simple-rest-api-php).