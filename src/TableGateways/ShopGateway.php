<?php

namespace Src\TableGateways;

class ShopGateway
{

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT
                name,
                street_name,
                street_number,
                latitude,
                longitude,
                postal_code,
                email,
                website_url,
                phone_number,
                mo_do_phone_availability,
                mo_do_phone_availability_from,
                mo_do_phone_availability_until,
                fr_phone_availability,
                fr_phone_availability_from,
                fr_phone_availability_until,
                shop_description,
                merchandise_managment_system_description,
                categories,
                shipping_pick_up,
                shipping_bicycle_delivery,
                shipping_postal,
                shipping_misc
            FROM
                shop
            LIMIT
                100
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(array $input)
    {
        $statement = "
            INSERT INTO shop (
                name,
                street_name,
                street_number,
                latitude,
                longitude,
                postal_code,
                email,
                website_url,
                phone_number,
                mo_do_phone_availability,
                mo_do_phone_availability_from,
                mo_do_phone_availability_until,
                fr_phone_availability,
                fr_phone_availability_from,
                fr_phone_availability_until,
                shop_description,
                merchandise_managment_system_description,
                categories,
                shipping_pick_up,
                shipping_bicycle_delivery,
                shipping_postal,
                shipping_misc
                )
            VALUES
            (
                :name,
                :street_name,
                :street_number,
                :latitude,
                :longitude,
                :postal_code,
                :email,
                :website_url,
                :phone_number,
                :mo_do_phone_availability,
                :mo_do_phone_availability_from,
                :mo_do_phone_availability_until,
                :fr_phone_availability,
                :fr_phone_availability_from,
                :fr_phone_availability_until,
                :shop_description,
                :merchandise_managment_system_description,
                :categories,
                :shipping_pick_up,
                :shipping_bicycle_delivery,
                :shipping_postal,
                :shipping_misc
            );
        ";
        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'name' => $input['name'],
                'street_name' => $input['street_name'],
                'street_number' => $input['street_number'],
                'latitude' => $input['latitude'],
                'longitude' => $input['longitude'],
                'postal_code' => $input['postal_code'],
                'email' => $input['email'],
                'website_url' => $input['website_url'],
                'phone_number' => $input['phone_number'],
                'mo_do_phone_availability' => $input['mo_do_phone_availability'],
                'mo_do_phone_availability_from' => $input['mo_do_phone_availability_from'],
                'mo_do_phone_availability_until' => $input['mo_do_phone_availability_until'],
                'fr_phone_availability' => $input['fr_phone_availability'],
                'fr_phone_availability_from' => $input['fr_phone_availability_from'],
                'fr_phone_availability_until' => $input['fr_phone_availability_until'],
                'shop_description' => $input['shop_description'],
                'merchandise_managment_system_description' => $input['merchandise_managment_system_description'],
                'categories' => $input['categories'],
                'shipping_pick_up' => $input['shipping_pick_up'],
                'shipping_bicycle_delivery' => $input['shipping_bicycle_delivery'],
                'shipping_postal' => $input['shipping_postal'],
                'shipping_misc' => $input['shipping_misc']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
