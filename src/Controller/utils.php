<?php

function update_geo_data($input)
{
  $url = create_url($input['street_name'], $input['postal_code']);
  $json = curl_get_contents($url);

  $response = json_decode($json, true);

  // if multiple return values, pick first
  $input['latitude'] = $response[0]['lat'];
  $input['longitude'] = $response[0]['lon'];

  return $input;
}

// use php-curl to get contents
function curl_get_contents($url)
{
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_REFERER, 'https://nachbarschaftsmartkplatz.de');

  $data = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  curl_close($ch);

  return ($httpcode >= 200 && $httpcode < 300) ? $data : false;
}

function create_url($address, $plz)
{
  $url = "https://nominatim.openstreetmap.org/search/";
  $url .= rawurlencode($address . " " . $plz);
  $url .= "?format=json";
  return $url;
}
