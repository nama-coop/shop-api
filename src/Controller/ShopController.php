<?php
namespace Src\Controller;

require __DIR__ . '/utils.php';

use Src\TableGateways\ShopGateway;

class ShopController {

    private $requestMethod;
    private $shopGateway;

    public function __construct($db, $requestMethod)
    {
        $this->requestMethod = $requestMethod;

        $this->shopGateway = new ShopGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'OPTIONS':
                $response = $this->corsResponse();
                break;
            case 'GET':
                $response = $this->getAllShops();
                break;
            case 'POST':
                $response = $this->createShopFromRequest();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllShops()
    {
        $result = $this->shopGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createShopFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateShop($input)) {
            return $this->unprocessableEntityResponse();
        }
        try {
            $input = update_geo_data($input);
        } catch (\Exception $e) {
            exit($e->getMessage());
        }
        $this->shopGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function validateShop($input)
    {
        if (! isset($input['name'])) {
            return false;
        }
        if (! isset($input['street_name'])) {
            return false;
        }
        if (! isset($input['postal_code'])) {
            return false;
        }
        if (! isset($input['email'])) {
            return false;
        }
        if (! isset($input['categories'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }

    private function corsResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 200 Success';
        $response['body'] = null;
        return $response;
    }
}
